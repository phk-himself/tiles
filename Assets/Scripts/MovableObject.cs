﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovableObject : MonoBehaviour {

	public bool updatedPath = false;
	public List<Node> currentPath = null;
	public float moveTime = 0.5f;
	private float inverseMoveTime;
	public TileMap map;
	public bool moving = false;
	// Use this for initialization
	void Start () {
		inverseMoveTime = 1f / moveTime;
	}
	public void updatePath(List<Node> newPath){

		currentPath = newPath;
		updatedPath = true;
	}
	// Update is called once per frame
	void Update () {

		if (currentPath != null && !moving && currentPath.Count > 0) {
			moving = true;
			Node nextNode = currentPath[0];
			Vector3 destination = new Vector3(nextNode.x, nextNode.y,0f);
			Debug.Log("moving to "+destination);
			StartCoroutine (SmoothMovement(destination));
			currentPath.Remove(nextNode);
		}



		if (currentPath != null) {
			int currNode =0;

			while(currNode < currentPath.Count-1){
				Vector3 start = new Vector3(currentPath[currNode].x+0.5f,currentPath[currNode].y+0.5f,0.2f);
				Vector3 end   = new Vector3(currentPath[currNode+1].x+0.5f,currentPath[currNode+1].y+0.5f,0.2f);
				Debug.DrawLine(start,end, Color.red);
				currNode++;
			}
			updatedPath = false;
		}

	}

	protected IEnumerator SmoothMovement (Vector3 end)
	{
		//Calculate the remaining distance to move based on the square magnitude of the difference between current position and end parameter. 
		//Square magnitude is used instead of magnitude because it's computationally cheaper.
		float sqrRemainingDistance = (transform.position - end).sqrMagnitude;
		
		//While that distance is greater than a very small amount (Epsilon, almost zero):
		while(sqrRemainingDistance > float.Epsilon)
		{
			//Find a new position proportionally closer to the end, based on the moveTime

			float modifiedInverseMoveTime =  inverseMoveTime / map.costToMove((int)transform.position.x,(int)transform.position.y);
			Vector3 newPosition = Vector3.MoveTowards(this.transform.position, end, modifiedInverseMoveTime * Time.deltaTime);
			
			//Call MovePosition on attached Rigidbody2D and move it to the calculated position.
			this.transform.position = newPosition;
			
			//Recalculate the remaining distance after moving.
			sqrRemainingDistance = (transform.position - end).sqrMagnitude;
			
			//Return and loop until sqrRemainingDistance is close enough to zero to end the function
			yield return null;
		}

		moving = false;
	}
}
