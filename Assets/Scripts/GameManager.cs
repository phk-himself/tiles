using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameManager : MonoBehaviour {
	//MapVariables
	public int width;
	public int height;
	public float tileSize;
	public int tileResolution;
	public Texture2D terrainTiles;

	List<Node> currentPath;


	TileMapMeshRender meshRenderer;
	TileMap map;
	GameObject unitGO; 
	MovableObject unit;
	GameObject mainCameraGO;
	Camera mainCamera;

	// Use this for initialization
	void Start () {
		//Initialise map
		//Random.seed = 1;
		map = new TileMap (width,height);
		mainCameraGO = GameObject.Find ("Main Camera");
		mainCamera = mainCameraGO.GetComponent<Camera> ();
		mainCamera.GetComponent<CameraControl>().camHeight = height;
		mainCamera.GetComponent<CameraControl>().camWidth = width;
		//Generate pathfinding graph

		//Find unit and place it on first floor tile.
		unitGO = GameObject.Find ("Unit");
		unit = unitGO.GetComponent<MovableObject>();
		int startPosX=0;
		int startPosY=0;
		bool found =false;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if(map.getTileTypeAt(x,y) == (int) MapTile.TILE_TYPE.GRASS || map.getTileTypeAt(x,y) == (int) MapTile.TILE_TYPE.SAND)
				{
					startPosX = x;
					startPosY = y;
					found = true;
					Debug.Log("Found first walkable tile at X="+startPosX+" Y="+startPosY);
					break;
				}

			}
			if(found)
				break;
		}
		unitGO.transform.position = new Vector3 ((float)startPosX, (float)startPosY, 0);
		mainCameraGO.transform.position = new Vector3(unit.transform.position.x,unit.transform.position.y,-1f);
		unit.map = map;




		//Get meshRenderer object and draw to screen.
		GameObject go = GameObject.Find ("TileMapMeshRender");
		go.transform.position = new Vector3(0,height,0);
		meshRenderer = go.GetComponent<TileMapMeshRender> ();
		meshRenderer.SetupMesh (width, height, tileSize, tileResolution, terrainTiles);
		meshRenderer.UpdateMap (map);
		meshRenderer.BuildMesh ();


	}
	

	void Update () {


		if (Input.GetButtonDown("Fire1")) {
			Vector3 tilePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
			int tileX = (int)Mathf.Floor(tilePosition[0]);
			int tileY = (int)Mathf.Floor(tilePosition[1]);
			if(tileX >= 0 && tileX <= width && tileY >=0 && tileY <= height){
				if (map.getTileAt(tileX,tileY).isWalkable())
					unit.updatePath(map.findPathToTile((int)unitGO.transform.position.x,(int)unitGO.transform.position.y,tileX,tileY));
			}
		}
		//update view
	}

	//Pathfinding



}
