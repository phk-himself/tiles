﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {
	
	public int Boundary  = 50; // distance from edge scrolling starts
	public int speed = 5;
	public int minZoom;
	public int maxZoom;
	private int theScreenWidth;
	private int theScreenHeight;
	public int camWidth;
	public int camHeight;
	Camera cam;
	 void Start() 
	{
		theScreenWidth = Screen.width;
		theScreenHeight = Screen.height;
		cam = this.GetComponent<Camera>();
	}
	
	void Update() 
	{

		if (Input.mousePosition.x > theScreenWidth - Boundary)
		{
			transform.position += new Vector3(speed * Time.deltaTime,0,0); // move on +X axis
		}
		
		if (Input.mousePosition.x < 0 + Boundary)
		{
			transform.position -= new Vector3(speed * Time.deltaTime,0,0); // move on -X axis
		}
		
		if (Input.mousePosition.y > theScreenHeight - Boundary)
		{
			transform.position += new Vector3(0,speed * Time.deltaTime,0); // move on +Z axis
		}
		
		if (Input.mousePosition.y < 0 + Boundary)
		{
				transform.position -= new Vector3(0,speed * Time.deltaTime,0); // move on -Z axis
		}
		if (Input.GetAxis("Mouse ScrollWheel") < 0) // back 
		{ 
			Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize-1, minZoom);
			
		}
		if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
		{
		Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize+1, maxZoom);
		}
		if (Input.GetMouseButtonDown (1)) 
		{
			transform.position = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,transform.position.z));
		}

		//Bound camera.
		transform.position = new Vector3 (Mathf.Max (0f, transform.position.x), Mathf.Max (0f, transform.position.y), transform.position.z);
		transform.position = new Vector3 (Mathf.Min (camWidth, transform.position.x), Mathf.Min (camHeight, transform.position.y), -1);
	
}   
}