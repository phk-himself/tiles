using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class TileMapMeshRender : MonoBehaviour {

	int size_x;
	int size_y;
	float tileSize;
	int tileResolution;
	Texture2D terrainTiles;
	TileMap tileMap;
	Color[][] choppedTiles;


	public void SetupMesh(int size_x, int size_y, float tileSize, int tileResolution, Texture2D terrainTiles){
		this.size_x = size_x;
		this.size_y = size_y;
		this.tileSize = tileSize;
		this.tileResolution = tileResolution;
		this.terrainTiles = terrainTiles;
		this.choppedTiles = ChopUpTiles ();
	}

	public void UpdateMap(TileMap map){
		this.tileMap = map;
	}

	Color[][] ChopUpTiles() {
		int numTilesPerRow = terrainTiles.width / tileResolution;
		int numRows = terrainTiles.height / tileResolution;
		
		Color[][] tiles = new Color[numTilesPerRow*numRows][];
		
		for(int y = 0; y < numRows; y++) {
			for(int x = 0; x < numTilesPerRow; x++) {
				tiles[y * numTilesPerRow + x] = terrainTiles.GetPixels( x * tileResolution , y * tileResolution, tileResolution, tileResolution );
			}
		}
		
		return tiles;
	}


	void BuildTexture(){
		int texWidth = size_x * tileResolution;
		int texHeight = size_y * tileResolution;
		Texture2D texture = new Texture2D (texWidth, texHeight);


		for(int y=0; y < size_y; y++) {
			for(int x=0; x < size_x; x++) {
				Color[] p = choppedTiles[tileMap.getTileTypeAt(x,y)];
				texture.SetPixels(x*tileResolution, y*tileResolution, tileResolution, tileResolution, p);
			}
		}

		texture.filterMode = FilterMode.Point;
		texture.wrapMode = TextureWrapMode.Clamp;
		texture.Apply ();

		MeshRenderer meshRenderer = GetComponent<MeshRenderer> ();
		meshRenderer.sharedMaterial = new Material(Shader.Find("Sprites/Default"));
		meshRenderer.sharedMaterials[0].mainTexture = texture;


	}


	public void BuildMesh () {

		//size variables
		int verticesSizeX = size_x + 1;
		int verticesSizeY = size_y + 1;
		int numTiles = size_x * size_y;
		int numVertices = verticesSizeX * verticesSizeY;
		int numTriangles = numTiles * 2;

		//generate verts normals and uv
		Vector3[] vertices = new Vector3[numVertices];
		Vector3[] normals = new Vector3[numVertices];
		Vector2[] uv = new Vector2[numVertices];
		for(int y = 0; y < verticesSizeY; y++){
			for(int x = 0; x < verticesSizeX; x++){
				vertices[y*verticesSizeX+x] = new Vector3(x * tileSize,-y*tileSize,0);
				normals[y*verticesSizeX+x] = Vector3.up;
				uv[y*verticesSizeX+x] = new Vector2( (float) x / size_x, 1f - (float)y/size_y);
			}
		}

		//generate triangles
		int[] triangles = new int[numTriangles*3];
		for(int y=0; y < size_y; y++) {
			for(int x=0; x < size_x; x++) {
				int squareIndex = y * size_x + x;
				int triOffset = squareIndex * 6;
				triangles[triOffset + 0] = y * verticesSizeX + x + 		   0;
				triangles[triOffset + 2] = y * verticesSizeX + x + verticesSizeX + 0;
				triangles[triOffset + 1] = y * verticesSizeX + x + verticesSizeX + 1;

				triangles[triOffset + 3] = y * verticesSizeX + x + 		   0;
				triangles[triOffset + 5] = y * verticesSizeX + x + verticesSizeX + 1;
				triangles[triOffset + 4] = y * verticesSizeX + x + 		   1;
			}
		}

		//create and populate mesh
		Mesh mesh = new Mesh ();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.normals = normals;
		mesh.uv = uv;

		//initialise mesh components
		MeshFilter meshFilter = GetComponent<MeshFilter> ();
		MeshCollider meshCollider = GetComponent<MeshCollider> ();
		meshFilter.mesh = mesh;
		meshCollider.sharedMesh = mesh;

		//Set Texture
		BuildTexture ();
	}
}
