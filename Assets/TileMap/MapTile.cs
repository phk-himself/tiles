﻿
public class MapTile{
	
	public enum TILE_TYPE{
		GRASS,
		WATER,
		SAND,
		WALL,
		UNKNOWN
	}

	TILE_TYPE tileType;
	bool walkable;
	float walkCost;

	public MapTile(TILE_TYPE tileType)
	{
		this.tileType = tileType;
		if (tileType == TILE_TYPE.WALL || tileType == TILE_TYPE.UNKNOWN) {
			this.walkCost = 0f;
			this.walkable = false;
		} else if (tileType == TILE_TYPE.GRASS) {
			this.walkCost = 1f;
			this.walkable = true;
		} else {
			this.walkCost = 2f;
			this.walkable = true;
		}

	}

	public TILE_TYPE getTileType(){
		return this.tileType;
	}
	public bool isWalkable(){
		return this.walkable;
	}
	public float getWalkCost(){
		return this.walkCost;
	}
	public bool isFloor(){
		return (this.tileType == TILE_TYPE.GRASS || this.tileType == TILE_TYPE.SAND);
	}
	public void setTileType(TILE_TYPE tileType){
		this.tileType = tileType;
	}
}
