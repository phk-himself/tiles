﻿using UnityEngine;
using System.Collections.Generic;

public class TileMap {

	MapTile[,] map;
	int width;
	int height;
	List<Room> rooms;
	Node[,] pathfindingGraph;

	// PATHFINDING
	void generatePathFindingGraph(){
		
		//Generate nodes
		pathfindingGraph = new Node[width, height];
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				pathfindingGraph[x,y] = new Node();
				pathfindingGraph[x,y].x = x;
				pathfindingGraph[x,y].y = y;
			}
		}
		
		//Generate neighbours
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				
				//4 way connection
				//left
				if(x > 0)
					pathfindingGraph[x,y].neighbours.Add(pathfindingGraph[x-1,y]);
				//right
				if(x < width - 1)  
					pathfindingGraph[x,y].neighbours.Add(pathfindingGraph[x+1,y]);
				//bottom
				if(y > 0)
					pathfindingGraph[x,y].neighbours.Add(pathfindingGraph[x,y-1]);
				//top
				if(y < height - 1)
					pathfindingGraph[x,y].neighbours.Add(pathfindingGraph[x,y+1]);
			}
		}
	}

	// Find a path between source and tile.
	public List<Node> findPathToTile(int sourceX, int sourceY, int tileX, int tileY){

		//Dijkstra algorithm
		Dictionary<Node, float> dist = new Dictionary<Node, float>();
		Dictionary<Node, Node> prev = new Dictionary<Node, Node> ();
		
		Node source = pathfindingGraph[sourceX,sourceY];
		Node target = pathfindingGraph [tileX, tileY];
		dist [source] = 0;
		prev [source] = null;
		List<Node> toVisit = new List<Node> ();
		
		//Initialise infinity distance for each node
		foreach (Node v in pathfindingGraph) {
			if(v != source){
				dist[v] = Mathf.Infinity;
				prev[v] = null;
			}
			toVisit.Add(v);
		}

		//Check best possiblity.
		while (toVisit.Count > 0) {
			//Find the closest node to the current one
			Node u = null;
			foreach(Node candidateU in toVisit){
				if( u == null || dist[candidateU] < dist[u]){
					u = candidateU;
				}
				
			}
			
			//found target!
			if(u == target){
				break;
			}
			
			

			foreach(Node v in u.neighbours){
				float alt = dist[u] + costToMove(v.x,v.y);
				if( alt < dist[v]){
					dist[v] = alt;
					prev[v] = u;
				}
			}
			toVisit.Remove(u);
			
		}
		//either found target or no route!
		if (prev [target] == null) {
			//no route to target
			Debug.Log("No route!");
			return null;
		}
		//find path
		List<Node> path = new List<Node>();
		Node curr = target;
		while(curr != null){
			path.Add(curr);
			curr = prev[curr];
		}
		//reorder path
		path.Reverse();
		
		
		return path;
	
	}


	public float costToMove(int destX, int destY){

		float cost;
		if (map [destX, destY].isWalkable())
			cost = map [destX, destY].getWalkCost();
		else
			cost = Mathf.Infinity;
		return cost;
	}
	
	public int getTileTypeAt(int x, int y){
		return (int)(this.map [x, y].getTileType());
	}
	public MapTile getTileAt(int x, int y){
		return (this.map [x, y]);
	}
	public void setTileTypeAt(int x, int y, int typeTile){
		map [x, y].setTileType ((MapTile.TILE_TYPE)typeTile);
	}

	//Create a map with rooms of width x height.
	public TileMap(int width, int height){
		Room r;
		this.width = width;
		this.height = height;
		this.map = new MapTile[width,height];

		//Initialise map as unknown
		for(int x = 0; x < width; x++)
			for(int y = 0; y < height; y++)
				map[x,y] = new MapTile(MapTile.TILE_TYPE.UNKNOWN);

		rooms = new List<Room>();
		
		int maxFails = 10;
		
		while(rooms.Count < 20) {
			int rsx = Random.Range(4,14);
			int rsy = Random.Range(4,10);
			
			r = new Room();
			r.left = Random.Range(0, width - rsx);
			r.top = Random.Range(0, height-rsy);
			r.width = rsx;
			r.height = rsy;
			
			if(!RoomCollides(r)) {			
				rooms.Add (r);
			}
			else {
				maxFails--;
				if(maxFails <=0)
					break;
			}
			
		}
		
		foreach(Room r2 in rooms) {
			MakeRoom(r2);
		}
		
		
		for(int i=0; i < rooms.Count; i++) {
			if(!rooms[i].isConnected) {
				int j = Random.Range(1, rooms.Count);
				MakeCorridor(rooms[i], rooms[(i + j) % rooms.Count ]);
			}
		}
		
		MakeWalls();
		generatePathFindingGraph ();
	}


	//Check if this room collides with any other
	bool RoomCollides(Room r) {
		foreach(Room r2 in rooms) {
			if(r.CollidesWith(r2)) {
				return true;
			}
		}
		
		return false;
	}



	void MakeRoom(Room r) {
		MapTile.TILE_TYPE floorMaterial;
		if (Random.Range (0, 4) <= 1)
			floorMaterial = MapTile.TILE_TYPE.SAND;
		else
			floorMaterial = MapTile.TILE_TYPE.GRASS;
		for(int x=0; x < r.width; x++) {
			for(int y=0; y < r.height; y++){
				if(x==0 || x == r.width-1 || y==0 || y == r.height-1) {
					map[r.left+x,r.top+y] = new MapTile(MapTile.TILE_TYPE.WALL);
				}
				else {
					map[r.left+x,r.top+y] = new MapTile(floorMaterial);
				}
			}
		}
		
	}
	void MakeCorridor(Room r1, Room r2) {
		MapTile.TILE_TYPE floorMaterial = MapTile.TILE_TYPE.GRASS;

		int x = r1.center_x;
		int y = r1.center_y;
		bool crossesRoom = false;

		foreach (Room currentRoom in rooms)
		{
			if(currentRoom.isInRoom(x,y))
			{
				crossesRoom = true;

			}
		}
		while( x != r2.center_x) {
			if(!crossesRoom)
				map[x,y] = new MapTile(floorMaterial);
			
			x += x < r2.center_x ? 1 : -1;
		}
		
		while( y != r2.center_y ) {
			if(!crossesRoom)
				map[x,y] = new MapTile(floorMaterial);
			
			y += y < r2.center_y ? 1 : -1;
		}
		
		r1.isConnected = true;
		r2.isConnected = true;
		
	}
	
	void MakeWalls() {
		for(int x=0; x< width;x++) {
			for(int y=0; y< height;y++) {
				if(map[x,y].getTileType()==MapTile.TILE_TYPE.UNKNOWN && HasAdjacentFloor(x,y)) {
					map[x,y]=new MapTile(MapTile.TILE_TYPE.WALL);
				}
			}
		}
	}
	
	bool HasAdjacentFloor(int x, int y) {
		if( x > 0 && map[x-1,y].isFloor() )
			return true;
		   if( x < width-1 && map[x+1,y].isFloor())
			return true;
		   if( y > 0 && map[x,y-1].isFloor())
			return true;
		if( y < height-1 && map[x,y+1].isFloor())
			return true;
		
		if( x > 0 && y > 0 && map[x-1,y-1].isFloor())
			return true;
		if( x < width-1 && y > 0 && map[x+1,y-1].isFloor() )
			return true;
		
		if( x > 0 && y < height-1 && map[x-1,y+1].isFloor())
			return true;
		if( x < width-1 && y < height-1 && map[x+1,y+1].isFloor())
			return true;
		
		
		return false;
	}

	protected class Room {
		public int left;
		public int top;
		public int width;
		public int height;
		
		public bool isConnected=false;
		
		public int right {
			get {return left + width - 1;}
		}
		
		public int bottom {
			get { return top + height - 1; }
		}
		
		public int center_x {
			get { return left + width/2; }
		}
		
		public int center_y {
			get { return top + height/2; }
		}
		
		public bool CollidesWith(Room other) {
			if( left > other.right-1 )
				return false;
			
			if( top > other.bottom-1 )
				return false;
			
			if( right < other.left+1 )
				return false;
			
			if( bottom < other.top+1 )
				return false;
			
			return true;
		}
		public bool isInRoom(int x, int y){
			
			return false;
			//return (x > left+1 && x < right && y > top && y < bottom);
		}
		
	}
}

